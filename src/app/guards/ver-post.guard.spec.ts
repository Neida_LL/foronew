import { TestBed } from '@angular/core/testing';

import { VerPostGuard } from './ver-post.guard';

describe('VerPostGuard', () => {
  let guard: VerPostGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(VerPostGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

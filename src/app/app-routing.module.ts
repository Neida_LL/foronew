import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import { Inicio1Component } from './components/inicio1/inicio1.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { MyspaceGuard } from './guards/myspace.guard';

const routes: Routes = [
  {path:'login', component: LoginComponent},
  {path:'register', component: RegistroComponent},
  {path:'inicio/space/:id', component: NavbarComponent,canActivate: [MyspaceGuard]},
  {path:'inicio',
   loadChildren:()=>import('./components/dashboard/dashboard.module').then(x => x.DashboardModule)
  },
  {path:'**', pathMatch:'full',redirectTo:'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

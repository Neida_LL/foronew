import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';
import { MenuService } from 'src/app/services/menu.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Menu } from "..//../../interface/menu.interface";
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  activo:boolean = false;
  idM!:string;
  userG!:UsuarioI;
  menu: Menu[]=[];
  constructor(private _menuService: MenuService,
              private _serviceUser:UsuarioService,
              private activeRouted: ActivatedRoute,
              private router:Router) {
                this.activeRouted.params.subscribe(params => {
                  this.idM = params['id'];
                  console.log(this.idM);
                });           
                    this.cargarUserGlobal();    
      
              }

  ngOnInit(): void {
    this.cargarMenu();
  }

  cargarUserGlobal(){
    // this._serviceUser.setUsuarioGlobal(usuario);
    // this.userG = this._serviceUser.getUserG();
    // this.activo =true;
    
     if(!localStorage.getItem("usuarioGlobal")){
       this.activo =false;
     }else{
       let guardados = localStorage.getItem('usuarioGlobal');
       this.userG = JSON.parse(guardados || '{}');
       this.activo =true;

     } 
  }

  cargarMenu(){
    this._menuService.getMenu().subscribe(data =>{
      this.menu= data;
      console.log(this.menu);
    })
  }

  logout(){
    // this._serviceUser.dropUsuarioGlobal();
     localStorage.removeItem('usuarioGlobal');
    this.cargarUserGlobal();
    this.router.navigate(['/inicio']);        
  }


  
}



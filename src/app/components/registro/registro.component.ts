import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup ,Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';

import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  button:boolean = true ;
  buttonV:boolean = false ;
  form!:FormGroup;
  loading:boolean = false;


  constructor(private fb:FormBuilder, 
              private _snackbar: MatSnackBar, 
              private router:Router ,
              private serviceUser:UsuarioService) { 
  this.formulario();
  }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      usuario:['',[Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      password:['',[Validators.required,Validators.pattern(/^[0-9-a-zA-ZñÑ\s]+$/)]],
      confirmpass:['',[Validators.required,Validators.pattern(/^[0-9-a-zA-ZñÑ\s]+$/)]]
    });
  }


  compararPass(evento :any){
    // if ( this.form.value.password ==  this.form.value.confirmpass  && this.form.valid ) {
    //   this.button = true;
    // }

    let valor1 = this.form.value.password ;
    let valor2 = this.form.value.usuario ;


    if( this.form.valid && this.form.value.confirmpass === valor1 && !this.serviceUser.listaNamesUsers().includes(valor2)) {
      this.button = false;
      
    } else {
      this.buttonV = false;
        this.button = true;
    }
  }

  compararUsuario(evento :any){

    let valor1 = this.form.value.usuario ;
    if( this.buttonV =true && this.form.valid && !this.serviceUser.listaNamesUsers().includes(valor1)) {
        this.buttonV  = true;
        this.button  = false;

    } else {
        this.buttonV = false;
        this.button = true;

    }
  }




  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading=false;
    //Redireccionamos al dashboard
      this.router.navigate(['main'])      
    }, 1500);
  }
 

  ingresar(){
    const Usuario:UsuarioI ={
      usuario: this.form.value.usuario,
      password: this.form.value.password,
      fecha: new Date()+'',
      ultima_coneccion:'',
      cant_like:0,
      cant_dislike:0,
      lista_public:[]
    }    
    
    if (this.form.valid) {
       
      this.fakeLoading();
      this.serviceUser.agregarUserList(Usuario);
      this.serviceUser.setUsuarioGlobal(Usuario.usuario);      
      this.router.navigate(['inicio/user/',Usuario.usuario]);

    } else {
      this.error();
      this.form.reset();
    }
  
  }


  error():void{
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }
  
  makeRandomId(length = 4) {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  }


}
